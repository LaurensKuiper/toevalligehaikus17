#!/usr/bin/python3

import sys
from graphics import *
from button import *
import random
import tweepy
import time
import re
import subprocess

def syllable_counter(word):
    """Syllables of the input(word) will be counted"""
    klinkers = "aeiou"
    medeklinkers = "bcdfghjklmnpqrstvwxyz"
    s = ""
    k = ""
    m = ""
    l = 0
    for char in word:
       if char in medeklinkers:
           m = m + char
           s = s + char
           if k != "":
               k = ""
               m = ""
               l = l + 1
               s = ""
       if char in klinkers:
           k = k + char
           s = s + char
       if len(k) > 2:
           if k[0] == k[1] and k[0] == k[2]:
               l = l + 1
       if char in "äëïöü":
           l = l + 1
    mcounter = 0
    if len(word) <= 3:
        for char in word:
            if char in medeklinkers:
                mcounter = mcounter + 1
        if word[0] == word[1]:
            mcounter = mcounter -1
        if len(word) == 3:
            if word[0] == word[2]:
                mcounter = mcounter -1
            if word[1] == word[2]:
                mcounter = mcounter -1
        l = l + mcounter
    return l

def main():

    """Opens up GUI with buttons for everything this function can do"""
    #everything that is needed to use the tweepy module and post a tweet on the account
    consumer_key = '7e7WtNQYMxU9U7sFmQgpAThDj'
    consumer_secret = '1OPrmJiGupZ9kRMH47vnYBTwrFLgx83HO2XUH2G5O35DFKtCYy'
    acces_key = '851438199226421248-LDyi0riBQ9TDijjorZtrCkF6fgei8xe'
    acces_secret = 'zhl93tzYowi1GNrhdfxekeg6DA96b3DzrjmfGAPv4sIgs'
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(acces_key, acces_secret)
    api = tweepy.API(auth)

    #everything of the graphical interface that will be shown
    win = GraphWin("Window", 500, 500)
    win.setBackground(color_rgb(192, 222, 237))

    frame = Rectangle(Point(20, 80), Point(480, 280))
    frame.setFill("white")
    frame.draw(win)

    message = Text(Point(250, 145), "Welcome!")
    message.setFace("courier")
    message.setSize(16)
    message.draw(win)

    tweets = Text(Point(80,350), "Tweets processed:")
    tweets.setFace("courier")
    tweets.setSize(10)
    tweets.draw(win)

    countertw = Text(Point(80, 370), "0")
    countertw.setFace("courier")
    countertw.setSize(12)
    countertw.draw(win)
 
    date = Text(Point(250,30),"Date:")
    date.setFace("courier")
    date.setSize(12)
    date.draw(win)

    genButton = Button(win, Point(250, 350), 150, 90, "Generate Haiku!")
    genButton.activate()
    quitButton = Button(win, Point(205, 430), 60, 30, "Quit")
    quitButton.activate()
    shareButton = Button(win, Point(295, 430), 60, 30, "Share")
    shareButton.activate()


    pt = win.getMouse()
    while not quitButton.clicked(pt):
        if genButton.clicked(pt):
            message.setText("Wait a moment please")
            #r1-r4 are used to pick a random date for the twitter data
            r1 = str(random.randrange(2011,2016))
            r2 = str(random.randrange(1,12))
            if int(r2) < 10:
                r2 = "0"+r2
            r3 = str(random.randrange(1,28))
            if int(r3) < 10:
                r3 = "0"+r3
            r4 = str(random.randrange(0,23))
            if int(r4) < 10:
                r4 = "0"+r4
            date.setText("Date: "+r1+"/"+r2+"/"+r3+" Hour: "+r4)
            goodstring = "/net/corpora/twitter2/Tweets/"+r1+"/"+r2+"/"+r1+r2+r3+":"+r4+".out.gz"
            #opening the chosen file of the twitter data and opening the tools that come with it, named tweet2tab. This tool will extract only the username and the text of the tweet.
            p1 = subprocess.Popen(["zless", goodstring], stdout=subprocess.PIPE)
            p2 = subprocess.Popen(["/net/corpora/twitter2/tools/tweet2tab", "-i", "user", "text"], stdin=p1.stdout, stdout=subprocess.PIPE)
            p1.stdout.close()
            output = p2.communicate()
            good = output[0].decode("utf-8")
            good2 = good.splitlines()
            #opening the file that will be used to look for the number of syllables in the word
            path = "/net/corpora/CELEX/dutch/dpw/dpw.cd"
            with open(path, "r") as ab:
                wordfile = ab.readlines()
            countlist = []
            tweetcount = 0
            for tweet in good2:
                tweetcount = tweetcount + 1
                #remove punctuation and numbers so the data is ready to be used
                clean = ' '.join(re.sub("([@#][A-Za-z]+)|([^A-Za-z \t])|(\w+:\/\/\S+)"," ",tweet).split())
                wordlist = clean.split()
                username = wordlist.pop(0)
                if "RT" in wordlist:
                    wordlist.remove("RT")
                totalcounter = 0
                countlist = []
                notinfile = 0
                for word in wordlist:
                    word = word.lower()
                    #first for each word check if it's in the corporus file and if it does, count number of syllables
                    for line in wordfile:
                        line2 = line.split("\\")
                        if word == line2[1]:
                            if len(word) == 1:
                                notinfile = notinfile + 1
                                break
                            counter = 1
                            counter = counter + line2[3].count('-')
                            totalcounter = totalcounter + counter
                            countlist.append(totalcounter)
                            break
                    #else use the own made syllable counter
                    else:
                        notinfile = notinfile + 1
                        sylnumber = syllable_counter(word)
                        totalcounter = totalcounter + sylnumber
                        countlist.append(totalcounter)
                    #because language follows rules but has a a lot of exceptions the number of words that are counted with the own syllable counter will be a maximum of two. This way more tweets are found with haikus in it because a lot of tweets only have one or two words that aren't in the corpus.
                    if notinfile > 2:
                        break
                    if totalcounter > 17:
                        break
                #then finally it will be checked if the tweet agrees the rules of a haiku and if it does it will be showed in the gui
                haikulist = []
                if 5 in countlist and 12 in countlist and 17 == countlist[-1] and len(wordlist) == len(countlist):
                    haiku1 = " ".join(wordlist[:int(countlist.index(5))+1])
                    haiku2 = " ".join(wordlist[int(countlist.index(5))+1:int(countlist.index(12))+1])
                    haiku3 = " ".join(wordlist[int(countlist.index(12))+1:int(countlist.index(17))+1])
                    haikustring = "Accidental haiku by :@"+username+"\n"+haiku1+"\n"+haiku2+"\n"+haiku3
                    message.setText(haikustring)
                    print(countlist, wordlist, tweet,notinfile)
                    break
                countertw.setText(tweetcount)
        #the possibility to share the funniest tweets on the haiku twitter account
        pt = win.getMouse()
        if shareButton.clicked(pt):
            api.update_status(haikustring)

if __name__ == "__main__":
    main()
